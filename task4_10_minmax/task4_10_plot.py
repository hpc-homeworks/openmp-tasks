import pandas as pd
import matplotlib.pyplot as plt
    
def plot_mode():
    df = pd.read_csv('benchmarks/task4_10_out.csv')
    df = df.pivot(index='nprocs', columns='mode', values='ex_time')
    df.plot(
        figsize=(10, 5),
        xlabel='Кол-во потоков', ylabel='Время работы (сек)')
    plt.savefig('benchmarks/task4_10_out.png')
    plt.show()
    
def plot_finish_create():
    df = pd.read_csv('benchmarks/task4_10_out.csv')
    df = df.pivot(index='nprocs', columns='mode', values=['create_time', 'finish_time'])
    df.plot(
        figsize=(10, 5),
        xlabel='Кол-во потоков', ylabel='Время (сек)')
    plt.savefig('benchmarks/task4_10_out_finish_create.png')
    plt.show()

if __name__ == '__main__':
    plot_mode()
    plot_finish_create()
