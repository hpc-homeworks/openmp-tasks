#!/bin/sh
set -ex

module load gcc/9

max_threads=16
path_to_code=$PWD/task4_10_minmax.cpp
path_to_exe=$PWD/task4_10_minmax.exe
out_csv_file=$PWD/benchmarks/task4_10_out.csv

run_mode(){
    mode="$1"
    # Build
    g++ -fopenmp -DMODE_$mode -g $path_to_code -o $path_to_exe

    # Run
    export N=10000
    for nprocs in $(seq 1 $max_threads)
    do
        export OMP_NUM_THREADS=$nprocs
        times=$( $path_to_exe )
        printf "%s\n" "$nprocs,$mode,$times" >> $out_csv_file
    done
}

bench(){
    # Set up out file
    if [[ -f "$out_csv_file" ]]
    then
        rm $out_csv_file  
    fi
    printf '%s\n' "nprocs,mode,create_time,finish_time,ex_time" >> $out_csv_file

    run_mode OUTER
    run_mode INNER
}

bench
