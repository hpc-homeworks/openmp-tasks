#include <iostream>
#include <cstdlib>
#include <climits>
#include <ctime>
#include <cstdlib>
#include <omp.h>

#define DEF_N 1000

using namespace std;

int** generate_matrix(int n)
{
	int** matrix = new int* [n];
	for (size_t i = 0; i < n; i++)
	{
		matrix[i] = new int[n];
		for (size_t j = 0; j < n; j++)
		{
			matrix[i][j] = rand() % 100;
            // cout << matrix[i][j] << " ";

		}
        // cout << endl;
	}
	return matrix;
}

int main()
{
    // #if defined(MODE_OUTER)
    // cout << "mode: OUTER" << endl;
    // #elif defined(MODE_INNER)
    // cout << "mode: INNER" << endl;
    // #endif

    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int i, j;   
    int curr_el, curr_min, max_el = -INT_MAX;

    /* Инициализация данных */
    int** matrix = generate_matrix(n);
    
    double time_start, time_end, duration;
    #ifdef _OPENMP
        // Точка времени: до начала паралл. области
        time_start = omp_get_wtime();

        double time_point_it_st, time_point_it_end;
        double time_point1, time_point2, dur_create = 0.0, dur_finish = 0.0;
    #else
        time_start = clock();
    #endif

    #if defined(MODE_OUTER)
    #pragma omp parallel shared(matrix, max_el) private(i, j, curr_min, curr_el)
    #endif
    {
        #if defined(MODE_OUTER)
        // Точка времени: готовность всех потоков
        #pragma omp barrier
        #pragma omp single
        time_point1 = omp_get_wtime();

        #pragma omp for
        #endif
        for (i=0; i < n; i++) {
            curr_min = INT_MAX;
            #if defined(MODE_INNER)
            // Точка времени: до начала паралл. области
            time_point_it_st = omp_get_wtime();
            #pragma omp parallel shared(matrix, curr_min, i) private(j, curr_el)
            #endif
            {
                #if defined(MODE_INNER)
                // Точка времени: готовность всех потоков
                #pragma omp barrier
                time_point1 = omp_get_wtime();

                #pragma omp for
                #endif
                for (j=0; j < n; j++) {
                    curr_el = matrix[i][j];
                    if ( curr_el < curr_min )
                        #if defined(MODE_INNER)
                        #pragma omp critical
                        if ( curr_el < curr_min )
                            curr_min = curr_el;
                        #else
                        curr_min = curr_el;
                        #endif 
                }
                #if defined(MODE_INNER)
                // Точка времени: начало завершения всех потоков
                #pragma omp barrier
                #pragma omp single
                time_point2 = omp_get_wtime();
                #endif
            }
            #if defined(MODE_INNER)
                // Точка времени: конец паралл. области
                time_point_it_end = omp_get_wtime();

                dur_create += time_point1 - time_point_it_st;
                dur_finish += time_point_it_end - time_point2;
            #endif
            if ( curr_min > max_el )
                #if defined(MODE_OUTER)
                #pragma omp critical
                if ( curr_min > max_el )
                    max_el = curr_min;
                #else
                max_el = curr_min;
                #endif              
        }

        #if defined(MODE_OUTER)
        // Точка времени: начало завершения всех потоков
        #pragma omp barrier
        #pragma omp single
        time_point2 = omp_get_wtime();
        #endif
    }

    #ifdef _OPENMP
        // Точка времени: конец паралл. области
        time_end = omp_get_wtime();
        
        #if defined(MODE_OUTER)
            dur_create = time_point1 - time_start;
            dur_finish = time_end - time_point2;
        #endif

        // cout << "Time creating: " << dur_create << endl;
        // cout << "Time finishing: " << dur_finish << endl;
        cout << dur_create << ',' << dur_finish << ',';

        duration = time_end - time_start;
    #else
        time_end = clock();
        duration = (time_end - time_start) / CLOCKS_PER_SEC;
    #endif

    /* Вывод программы */
    // cout << "Size of matrix: " << n << endl;
    // cout << "MaxMin element: " << max_el << endl;
	// cout << "Time: " << duration << " s" << endl;
    cout << duration << endl;

    return 0;
}
