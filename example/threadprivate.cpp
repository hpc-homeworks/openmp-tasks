#include <iostream>
#include <omp.h>

using namespace std;

int n;
#pragma omp threadprivate (n)
int main()
{
	int num;

	n = 1;

	#pragma omp parallel private(num) copyin(n)
	{
		num = omp_get_thread_num();
		#pragma omp critical
		cout << "num = " << num << ": n = " << n << endl;
		n = omp_get_thread_num();
		#pragma omp critical
		cout << "num = " << num << ": n = " << n << endl;
	}

	cout << "n = " << n << endl;
	
	#pragma omp parallel private(num)
	{
		num = omp_get_thread_num();
		#pragma omp critical
		cout << "num = " << num << ": n = " << n << endl;
	}
	return 0;
}


