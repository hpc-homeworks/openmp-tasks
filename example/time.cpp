#include <iostream>
#include <chrono>
#include <thread>
#include <omp.h>

using namespace std;

int main()
{
	double time_start = omp_get_wtime();
	#pragma omp parallel
	{
		if (omp_get_thread_num() == 0){
			int num_threads = omp_get_num_threads();
			cout << "Num threads: " << num_threads << endl;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	double time_end = omp_get_wtime();
	cout << "Time: " << (time_end - time_start) << endl;
	cout << "Accuracy: " << omp_get_wtick() << endl;
	
	return 0;
}
