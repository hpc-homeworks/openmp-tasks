#include <iostream>
#include <omp.h>

#define CHUNK 100
#define NMAX 1000
#define LIMIT 100 

using namespace std;

int main()
{
	int i, j;   
    float sum, total = 0;

    float a[NMAX][NMAX];

    /* Инициализация данных */
    for (i=0; i < NMAX; i++)
        for (j=0; j < NMAX; j++)
            a[i][j] = 1.0;
    
    // Или #pragma omp parallel for shared(a) private(i,j,sum)
    #pragma omp parallel shared(a) private(i,j,sum) reduction (+:total) if (NMAX>LIMIT)
    {
        #pragma omp for ordered schedule (dynamic, CHUNK) nowait
        for (i=0; i < NMAX; i++) {
            sum = 0;
            for (j=0; j < NMAX; j++)
                sum += a[i][j];
            // #pragma omp atomic
            total = total + sum;
            #pragma omp ordered
            printf ("Сумма строки %d равна %f\n", i, sum);
        }
        printf ("Поток номер %d завершен\n", omp_get_thread_num());
    }
    printf ("Общая сумма матрицы равна %f\n",total);

    return 0;
}
