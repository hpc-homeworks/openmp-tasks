#include <iostream>
#include <cfloat>
#include <omp.h>

#define NMAX 4

using namespace std;

int main()
{
	int i, j;   
    float sum;

    float a[NMAX][NMAX];
    float b[NMAX][NMAX];

    /* Инициализация данных */
    for (i=0; i < NMAX; i++)
        for (j=0; j < NMAX; j++)
            a[i][j] = 1.0;
    
    #pragma omp parallel shared(a) private(i,j,sum)
    {
        #pragma omp sections nowait
        {
            /* Вычисление сумм строк и общей суммы */
            for (i=0; i < NMAX; i++)
            {
                sum = 0;
                for (j=0; j < NMAX; j++)
                    sum += a[i][j];
                printf ("Сумма строки %d равна %f\n",i,sum);
            }
        }

        #pragma omp sections nowait
        {
            /* Копирование матрицы */
            for (i=0; i < NMAX; i++)
            {
                for (j=0; j < NMAX; j++)
                    b[i][j] = a[i][j];
                printf ("Строка %d скопирована в матрицу b\n",i);
            }
        }
    }

    return 0;
}
