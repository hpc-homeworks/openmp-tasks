**Потоки**:
- omp_get_thread_num - номер потока
- omp_get_num_threads - текущее кол-во
- omp_get_max_threads - макс. возмодное кол-во
- omp_get_num_procs - число вычислительных элементов (процессоров или
ядер), доступных приложению

Установить:
- для директивы parallel: num_threads(4)
- omp_set_num_threads - вызывается из после.области
- OMP_NUM_THREADS - переменная окружения
> Приоритет: пар.директивы -> функция библ. -> перем.окруж

- omp_set_dynamic(dynamic=true) - динамическое определение кол-ва потоков
- OMP_DYNAMIC - переменная окружения
- omp_get_dynamic


**Время**:
- omp_get_wtime - текущее время
- omp_get_wtick - точность

Перечень **параметров директивы parallel**:
- if (scalar_expression) - выполнять параллельно или посл.
- private (list) - локальные копии
- shared (list) - общая ссылка
- default (shared | none)
- firstprivate (list) - после выхода будет возвращено исходное значение
- reduction (operator: list) - коллективные операции
- copyin (list)
- num_threads (scalar_expression)

## Распараллеливанием по данным

**Параметрами директивы for** являются:
- schedule (type [,chunk]) - распределение итераций
- ordered - порядок выполнение итераций
- nowait - не ждать завершения цикла
- private (list)
- shared (list)
- firstprivate (list)
- lastprivate (list) - значение после выхода будет такое, как у последнего потока
- reduction (operator: list)

**Параметры schedule**:
- static - итерации делятся поровну или на блоки размером chunk и распределяются сразу между потоками
- dynamic - потокам выдаётся по chunk итераций (или 1), далее итерации распределяются в зависимости от того, как потоки будут освобождаться
- guided - тоже самое, что dynamic, но размер блока уменьшается экспоненциально (при этом не меньше chunk)
- runtime - определяется переменной окружения `OMP_SCHEDULE "dynamic,3"`
- auto

**Reduction**

Редуцированные переменные идут сначала, как private, а по итогу цикла редуцируются (сливаются в одну).

- operator: +, -, *, &, |, ^, &&, ||
- op: +, -, *, &, |, ^
- expression: не включает x
- Выражения:
    - `x = x <operator> <expression>`
    - `x = <expression> <operator> x`
    - `x <op>= <expression>`
    - `x++, ++x, x--, --x`

## Взаимоисключение

В OpenMP взаимоисключение может быть организовано при помощи неделимых (atomic) операций, механизма критических секций (critical sections) или специального типа семафоров – замков (locks).

**atomic**

```c++
#pragma omp atomic
x <operator>= <expression>
```
- operator: +, *, -, /, &, |, ^, >>, <<
- x++; или ++x; или x--; или --x;

Менее эффективно, чем редукция.

**critical**

```c++
#pragma omp critical [(name)]
 <block>
```

**Семафоры (lock)**

- omp_lock_t - переменная
- omp_init_lock(omp_lock_t *lock) - инициализировать
- omp_set_lock(omp_lock_t &lock) - установить
- omp_unset_lock (omp_lock_t &lock) - освободить
- omp_test_lock (omp_lock_t &lock) - проверить свободность, если свободен, то закрыть, если нет, то не блокироваться
- omp_destroy_lock(omp_lock_t &lock) - перевод замка в неинициализированное состояние

> Также имеются вложенные замки omp_nest_lock_t

## Распараллеливанием по задачам

**sections**
```c++
{
    #pragma omp section
    <блок_программы>
    #pragma omp section
    <блок_программы>
}
```
> Порядок вызова секций не определён

Параметры:
- private (list)
- firstprivate (list)
- lastprivate (list)
- reduction (operator: list)
- nowait 


## Расширенные возможности OpenMP

**single**

Директива single определяет блок параллельного фрагмента, который должен быть выполнен только одним потоком.

По умолчанию остальные потоки ждут.
```c++
#pragma omp single [<параметр> ...]
 <блок_программы>
```
Параметры:
- private (list)
- firstprivate (list)
- copyprivate (list) - копирование переменных в локальные остальных потоков
- nowait 

**master**

Директива master определяет фрагмент кода, который должен быть выполнен только основным потоком

```c++
#pragma omp master
 <блок_программы>
```

**barrier**

В OpenMP **синхронизация** может быть обеспечена при помощи замков или директивы barrier.
```c++
#pragma omp barrier
```

**flush**

Директива flush позволяет определить точку синхронизации, в которой системой должно быть обеспечено единое для всех потоков состояние
памяти

```c++
#pragma omp flush [(list)]
```


**threadprivate**

Полученные в потоках значения постоянных переменных сохраняются между параллельными фрагментами программы. 

> **copyin** - параметр parallel для инициализации из послед. области 

**Вложенны параллельные фрагменты**

По умолчанию для выполнения вложенных параллельных фрагментов создается столько же потоков, как и для параллельных фрагментов верхнего уровня

- omp_set_nested(nested=true)
- OMP_NESTED
- omp_get_nested

## Компиляция


**Условная компиляция**
```c++
#ifdef _OPENMP
 <OpenMP-зависимый программный код>
#endif
```
