#include <iostream>
#include <cfloat>
#include <omp.h>

#define NMAX 1000

using namespace std;

int main()
{
	int i, j;   
    float sum, smax = -DBL_MAX;

    float a[NMAX][NMAX];

    /* Инициализация данных */
    for (i=0; i < NMAX; i++)
        for (j=0; j < NMAX; j++)
            a[i][j] = 1.0;

    omp_lock_t lock;
    omp_init_lock(&lock);
    
    #pragma omp parallel shared(a) private(i,j,sum)
    {
        #pragma omp for
        for (i=0; i < NMAX; i++) {
            sum = 0;
            for (j=0; j < NMAX; j++)
                sum += a[i][j];
            if ( sum > smax )
                // #pragma omp critical
                omp_set_lock (&lock);
                if ( sum > smax )
                    smax = sum;
                omp_unset_lock (&lock);
            printf ("Сумма строки %d равна %f\n", i, sum);
        }
    }
    printf ("Максимальная сумма равна %f\n",smax);
    omp_destroy_lock (&lock);

    return 0;
}
