#include <iostream>
#include <omp.h>

using namespace std;

int main()
{
    /* Выделение параллельного фрагмента*/
	#pragma omp parallel num_threads(4)
	{
		cout << "Hello World" << endl;
	}/* Завершение параллельного фрагмента */
	
	return 0;
}
