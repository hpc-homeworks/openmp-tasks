#include <iostream>
#include <cstdlib>
#include <climits>
#include <ctime>
#include <cstdlib>
#include <omp.h>

#define DEF_N 1000

using namespace std;

int** generate_matrix(int n)
{
	int** matrix = new int* [n];
	for (size_t i = 0; i < n; i++)
	{
		matrix[i] = new int[n];
		for (size_t j = 0; j < n; j++)
		{
			matrix[i][j] = rand() % 100;
            // cout << matrix[i][j] << " ";

		}
        // cout << endl;
	}
	return matrix;
}

int main()
{
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int i, j;   
    int curr_el, curr_min, max_el = -INT_MAX;

    /* Инициализация данных */
    int** matrix = generate_matrix(n);
    
    double time_start, time_end, duration;
    #ifdef _OPENMP
        time_start = omp_get_wtime();
    #else
        time_start = clock();
    #endif
     
    #pragma omp parallel shared(matrix, max_el) private(i, j, curr_min, curr_el)
    {
        #pragma omp for
        for (i=0; i < n; i++) {
            curr_min = INT_MAX;
            for (j=0; j < n; j++) {
                curr_el = matrix[i][j];
                if ( curr_el < curr_min )
                    curr_min = curr_el;
            }
            if ( curr_min > max_el )
                #pragma omp critical
                if ( curr_min > max_el )
                    max_el = curr_min;                 
        }
    }
    #ifdef _OPENMP
        time_end = omp_get_wtime();
        duration = time_end - time_start;
    #else
        time_end = clock();
        duration = (time_end - time_start) / CLOCKS_PER_SEC;
    #endif

    /* Вывод программы */
    // cout << "Size of matrix: " << n << endl;
    // cout << "MaxMin element: " << max_el << endl;
	// cout << "Time: " << duration << " s" << endl;
    cout << duration << endl;

    return 0;
}
