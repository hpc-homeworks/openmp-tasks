#!/bin/sh
set -ex

module load gcc/9

omp_num_threads=8
path_to_code=$PWD/task4_5_11_minmax.cpp
path_to_exe=$PWD/task4_5_11_minmax.exe
out_csv_file=$PWD/benchmarks/task4_5_11_out.csv

run_mode(){
    mode="$1"
    chank="$2"
    # chank=$(expr $n / $omp_num_threads)
    
    export OMP_NUM_THREADS=$omp_num_threads
    
    if test -z "$chank"
    then
        export OMP_SCHEDULE="$mode"
    else
        export OMP_SCHEDULE="$mode,$chank"
    fi

    # Run
    for n in $(seq 1000 1000 10000)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,$mode,$ex_time" >> $out_csv_file
    done
}

bench_not_omp()
    # Build
    g++ -g $path_to_code -o $path_to_exe

    # Run
    for n in $(seq 1000 1000 10000)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,NOT_OMP,$ex_time" >> $out_csv_file
    done

bench_omp()
    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    run_mode static
    run_mode dynamic 1
    run_mode guided 1

bench(){
    # Set up out file
    if [[ -f "$out_csv_file" ]]
    then
        rm $out_csv_file  
    fi
    printf '%s\n' "n,mode,ex_time" >> $out_csv_file

    bench_not_omp
    bench_omp
}

bench 
