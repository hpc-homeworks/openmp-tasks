import pandas as pd
import matplotlib.pyplot as plt
    
def plot_mode():
    df = pd.read_csv('benchmarks/task4_5_11_out.csv')
    df = df.pivot(index='n', columns='mode', values='ex_time')
    df.plot(
        figsize=(10, 5),
        xlabel='Размер задачи (N)', ylabel='Время работы (сек)')
    plt.savefig('benchmarks/task4_5_11_out.png')
    plt.show()
    
def plot_S():
    df = pd.read_csv('benchmarks/task4_5_11_out.csv')
    df_omp = df[df['mode'] == 'dynamic'].reset_index()
    df = df[df['mode'] == 'NOT_OMP'].reset_index()
    S = df['ex_time'] / df_omp['ex_time']
    df_S = pd.DataFrame({
        'n': df['n'],
        'S': S
    }) 
    df_S.plot(figsize=(10, 5), x='n', y='S')
    df_S.to_csv('benchmarks/task4_5_11_out_S.csv')
    plt.savefig('benchmarks/task4_5_11_out_S.png')
    plt.show()

if __name__ == '__main__':
    plot_mode()
    plot_S()
