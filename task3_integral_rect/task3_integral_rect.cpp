#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <cstdlib>
#include <cmath>
// #include <sys/time.h>
#include <chrono>
#include <omp.h>

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;

#define DEF_N 1000

using namespace std;

int main()
{
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int i;

    /* Инициализация данных */
    float a = 1, b = 5, x;
    auto f = [](float x) { return std::log2(x);};

    /* Настройка алгоритма */
    float h = (b - a) / n;

    float curr_prod, res = 0.0;
    
    double duration;
    #ifdef _OPENMP
    double time_start, time_end;
    time_start = omp_get_wtime();
    #else
    // struct timeval time_start, time_end, dtv;
    // struct timezone tz;
    // gettimeofday(&time_start, &tz);
    auto time_start = Time::now();
    // time_start = clock();
    #endif
     
    #pragma omp parallel shared(f, a, h) private(i, x, curr_prod) reduction (+:res)
    {
        #pragma omp for
        for (i=0; i < n; i++) {
            x = a + i * h;
            curr_prod = f(x) * h;
            res = res + curr_prod;
        }
    }
    #ifdef _OPENMP
        time_end = omp_get_wtime();
        duration = time_end - time_start;
    #else
        // time_end = clock();
        // duration = (time_end - time_start) / CLOCKS_PER_SEC;
        // gettimeofday(&time_end, &tz);
        // dtv.tv_sec = time_end.tv_sec - time_start.tv_sec;
        // dtv.tv_usec = time_end.tv_usec - time_start.tv_usec;
        // if ( dtv.tv_usec < 0 ) { dtv.tv_sec--; dtv.tv_usec += 1000000; }
        // duration = dtv.tv_sec * 1000 + dtv.tv_usec / 1000;
        
        auto time_end = Time::now();
        fsec fs = time_end - time_start;
        duration = fs.count();
    #endif

    /* Вывод программы */
    // cout << "Size of vectors: " << n << endl;
    // cout << "Result: " << res << endl;
    // cout << "Time: " << duration << " s" << endl;
    cout << duration << endl;

    return 0;
}
