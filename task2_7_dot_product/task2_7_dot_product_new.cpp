#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <string>
#include <omp.h>

#define DEF_N 10
#define DEF_NUM_VEC 30

using namespace std;

void write_data(int n, int number_vectors)
{
    ofstream data ("data.txt");
    if (data.is_open())
    {
        for (int i=0; i < number_vectors; i++){
            for (int j=0; j < n; j++){
                data << rand() % 100 << ' ';
            }
            data << endl;
            for (int j=0; j < n; j++){
                data << rand() % 100 << ' ';
            }
            data << endl << endl;
        }
        data.close();
    }
}

int main()
{
    /* Настройка задачи */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int num_vec = DEF_NUM_VEC;
    char * ENV_NUM_VEC = getenv("NUM_VEC");
    if (ENV_NUM_VEC)
        num_vec = atoi(ENV_NUM_VEC);

    write_data(n, num_vec);

    /* Определение переменных */
    int i, j;   
    string curr_el, skip_line;
    int** a = new int*[num_vec]; 
    int** b = new int*[num_vec];
    int* res = new int[num_vec];
    
    ifstream data; data.open("data.txt");

    double time_start, time_end, duration;
    #ifdef _OPENMP
        time_start = omp_get_wtime();
    #else
        time_start = clock();
    #endif

    int* is_ready = new int[num_vec];
    for (i=0; i < num_vec; i++)
        is_ready[i] = 0;

    // #ifdef _OPENMP
    // omp_lock_t* lock = new omp_lock_t[num_vec];
    // for (i=0; i < num_vec; i++)
    //     omp_init_lock(&lock[i]);
    // #endif
    // cout << "START PROGRAM" << endl;
    #pragma omp parallel shared(a, b, res, data, num_vec, n) private(i, j, curr_el) num_threads(2)
    {
        // #ifdef _OPENMP
        // #pragma omp master
        // for (i=0; i < num_vec; i++)
        //     omp_set_lock(&lock[i]);
        // #endif
        // cout << "START PARALLEL" << endl;
        #pragma omp sections
        {
            #pragma omp section
            {
                // cout << "START READING" << endl;
                for (i=0; i < num_vec; i++)
                {
                    a[i] = new int[n];
                    b[i] = new int[n];
                    // cout << "START INIT: " << i << endl;
                    /* Инициализация пары векторов */
                    for (j=0; j < n; j++){
                        data >> curr_el;
                        a[i][j] = stoi(curr_el);
                    }
                    for (j=0; j < n; j++){
                        data >> curr_el;
                        b[i][j] = stoi(curr_el);
                    }
                    getline (data,skip_line);
                    // cout << "END INIT: " << i << endl;
                    #pragma omp flush
                    is_ready[i] = 1;
                    #pragma omp flush
                    // #ifdef _OPENMP
                    // omp_unset_lock (&lock[i]);
                    // #endif
                }
            }
            #pragma omp section
            {
                // cout << "START COUNTING" << endl;
                for (i=0; i < num_vec; i++)
                {
                    // cout << "WAIT: " << i << endl;
                    #pragma omp flush
                    while (!is_ready[i]){
                        
                        #pragma omp flush
                    }
                    #pragma omp flush
                    // #ifdef _OPENMP
                    // omp_set_lock (&lock[i]);
                    // #endif
                    // cout << "COUNT: " << i << endl;
                    /* Расчёт скалярного произведения для пары векторов */
                    res[i] = 0;
                    for (j=0; j < n; j++)
                        res[i] += a[i][j] * b[i][j];
                    // cout << "DONE: " << i << " RES: " << res[i] << endl;
                }
            }

        }
    }

    #ifdef _OPENMP
        time_end = omp_get_wtime();
        duration = time_end - time_start;
    #else
        time_end = clock();
        duration = (time_end - time_start) / CLOCKS_PER_SEC;
    #endif

    data.close();
    remove("data.txt");

    /* Вывод программы */
    // for (i=0; i < num_vec; i++)
    //     cout << res[i] << endl;
    cout << duration << endl;

    return 0;
}
