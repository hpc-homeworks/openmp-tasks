#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <string>
#include <omp.h>

#define DEF_N 10
#define DEF_NUM_VEC 4

using namespace std;

void write_data(int n, int number_vectors)
{
    ofstream data ("data.txt");
    if (data.is_open())
    {
        for (int i=0; i < number_vectors; i++){
            for (int j=0; j < n; j++){
                data << rand() % 100 << ' ';
            }
            data << endl;
            for (int j=0; j < n; j++){
                data << rand() % 100 << ' ';
            }
            data << endl << endl;
        }
        data.close();
    }
}

int main()
{
    /* Настройка задачи */
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int num_vec = DEF_NUM_VEC;
    char * ENV_NUM_VEC = getenv("NUM_VEC");
    if (ENV_NUM_VEC)
        num_vec = atoi(ENV_NUM_VEC);

    write_data(n, num_vec);

    /* Определение переменных */
    int i, j;   
    float curr_prod, res;
    string curr_el, skip_line;
    int* a = new int[n]; 
    int* b = new int[n];
    int* a_next = new int[n]; 
    int* b_next = new int[n];
    
    ifstream data; data.open("data.txt");

    double time_start, time_end, duration;
    #ifdef _OPENMP
        time_start = omp_get_wtime();
    #else
        time_start = clock();
    #endif

    /* Инициализация первой пары векторов */
    for (int j=0; j < n; j++){
        data >> curr_el;
        a[j] = stoi(curr_el);
    }
    for (int j=0; j < n; j++){
        data >> curr_el;
        b[j] = stoi(curr_el);
    }

    /* Итерации по парам векторов */
    for (int i=0; i < num_vec; i++)
    {
        #pragma omp parallel shared(a, b, a_next, b_next, data, i) private(j, curr_prod, curr_el, res) num_threads(2)
        {
            #pragma omp sections
            {
                #pragma omp section
                {
                    res = 0.0;
                    /* Расчёт скалярного произведения для текущей пары */
                    for (j=0; j < n; j++) {
                        curr_prod = a[j] * b[j];
                        res = res + curr_prod;         
                    }
                    // cout << res << ' ';
                }

                #pragma omp section
                {
                    if ( i != num_vec - 1 )
                    {
                        /* Инициализация следующей пары векторов */
                        for (j=0; j < n; j++){
                            data >> curr_el;
                            a_next[j] = stoi(curr_el);
                        }
                        for (j=0; j < n; j++){
                            data >> curr_el;
                            b_next[j] = stoi(curr_el);
                        }
                        getline (data,skip_line);
                    }
                }
            }
            #pragma omp single
            {
                a = a_next;
                b = b_next;
            }
        }
    }

    #ifdef _OPENMP
        time_end = omp_get_wtime();
        duration = time_end - time_start;
    #else
        time_end = clock();
        duration = (time_end - time_start) / CLOCKS_PER_SEC;
    #endif

    data.close();
    remove("data.txt");

    /* Вывод программы */
    cout << duration << endl;

    return 0;
}
