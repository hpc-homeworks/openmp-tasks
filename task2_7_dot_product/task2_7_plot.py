import pandas as pd
import matplotlib.pyplot as plt
    
def plot():  
    df = pd.read_csv('benchmarks/task2_7_out_new.csv')
    df['mode'] = df['with_openmp'].apply(lambda x: 'with_omp' if x == 1 else 'without_omp')
    df = df.pivot(index='num_vec', columns='mode', values='ex_time')
    df.plot(
        figsize=(10, 5),
        xlabel='Размер задачи (Кол-во векторов)', ylabel='Время работы (сек)')
    plt.xticks(list(df.index))
    plt.savefig('benchmarks/task2_7_out_new.png')
    plt.show()

if __name__ == '__main__':
    plot()
