#!/bin/sh
set -ex

module load gcc/9

path_to_code=$PWD/task2_7_dot_product_new.cpp
path_to_exe=$PWD/task2_7_dot_product_new.exe
out_csv_file=$PWD/benchmarks/task2_7_out_new.csv

bench(){
    # Set up out file
    if [[ -f "$out_csv_file" ]]
    then
        rm $out_csv_file
    fi
    printf '%s\n' "num_vec,with_openmp,ex_time" >> $out_csv_file

    export N=200000

    # Build
    g++ -g $path_to_code -o $path_to_exe

    # Run
    for num_vec in $(seq 2 2 40)
    do
        export NUM_VEC=$num_vec
        ex_time=$( $path_to_exe )
        printf "%s\n" "$num_vec,0,$ex_time" >> $out_csv_file
    done

    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    # Run
    for num_vec in $(seq 2 2 40)
    do
        export NUM_VEC=$num_vec
        ex_time=$( $path_to_exe )
        printf "%s\n" "$num_vec,1,$ex_time" >> $out_csv_file
    done

    rm $path_to_exe
}

bench
