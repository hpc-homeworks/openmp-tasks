#!/bin/sh
set -ex

module load gcc/9

max_threads=16
max_n=100000
step_n=10000
min_n=1000

bench_parallel(){
    # Set up out file
    if [[ -f "$out_parallel_csv_file" ]]
    then
        rm $out_parallel_csv_file  
    fi
    printf '%s\n' "nprocs,ex_time" >> $out_parallel_csv_file

    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    # Run
    export N=$max_n
    for nprocs in $(seq 1 $max_threads)
    do
        export OMP_NUM_THREADS=$nprocs
        ex_time=$( $path_to_exe )
        printf "%s\n" "$nprocs,$ex_time" >> $out_parallel_csv_file
    done
}

bench_n(){
    # Set up out file
    if [[ -f "$out_n_csv_file" ]]
    then
        rm $out_n_csv_file
    fi
    printf '%s\n' "n,with_openmp,ex_time" >> $out_n_csv_file

    # Build
    g++ -g $path_to_code -o $path_to_exe

    # Run
    for n in $(seq $min_n $step_n $max_n)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,0,$ex_time" >> $out_n_csv_file
    done

    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    # Run
    export OMP_NUM_THREADS=$omp_num_threads
    for n in $(seq $min_n $step_n $max_n)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,1,$ex_time" >> $out_n_csv_file
    done

    rm $path_to_exe
}

# source ./task1_max_element/.env
# source ./task2_dot_product/.env
source ./task3_integral_rect/.env
# source ./task4_minmax/.env

# bench_parallel
bench_n

