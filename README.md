# OpenMP-Tasks

Сборка на кластере:
1. Подключить модули:
    ```
    module load gcc/9
    module load make
    ```
2. Вызов компилятора с доп. препроцессингом: `g++ -fopenmp -g hello.cpp -o hello.exe`


Запуск:
- Простой: `./hello.exe`
- Через планировщик:
    1.  Составление job.sh файла:
        ```bash
        #!/bin/sh
        ./hello.exe
        ```
        Далее: `chmod +x job.sh`
    2.  Отправление в очередь: `sbatch job.sh`
