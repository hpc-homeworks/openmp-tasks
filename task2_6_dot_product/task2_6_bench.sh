#!/bin/sh
set -ex

module load gcc/9

max_threads=16
path_to_code=$PWD/task2_6_dot_product.cpp
path_to_exe=$PWD/task2_6_dot_product.exe
out_csv_file=$PWD/benchmarks/task2_6_out.csv

run_mode(){
    mode="$1"
    # Build
    g++ -fopenmp -DMODE_$mode -g $path_to_code -o $path_to_exe

    # Run
    export N=100000
    for nprocs in $(seq 1 $max_threads)
    do
        export OMP_NUM_THREADS=$nprocs
        ex_time=$( $path_to_exe )
        printf "%s\n" "$nprocs,$mode,$ex_time" >> $out_csv_file
    done
}

bench(){
    # Set up out file
    if [[ -f "$out_csv_file" ]]
    then
        rm $out_csv_file  
    fi
    printf '%s\n' "nprocs,mode,ex_time" >> $out_csv_file

    run_mode REDUCTION
    run_mode ATOMIC
    run_mode CRITICAL
    run_mode LOCK
}

bench
