#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <cstdlib>
#include <omp.h>

#define DEF_N 1000

using namespace std;

int main()
{
    // #if defined(MODE_REDUCTION)
    // cout << "mode: REDUCTION" << endl;
    // #elif defined(MODE_ATOMIC)
    // cout << "mode: ATOMIC" << endl;
    // #elif defined(MODE_CRITICAL)
    // cout << "mode: CRITICAL" << endl;
    // #elif defined(MODE_LOCK)
    // cout << "mode: LOCK" << endl;
    // #endif

    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int i;   
    float curr_prod, res = 0.0;

    /* Инициализация данных */
    float *a = new float[n];
    float *b = new float[n];
    for (i=0; i < n; i++){
        a[i] = rand() % 100;
        b[i] = rand() % 100;
    }

    #if defined(MODE_LOCK)
    omp_lock_t lock;
    omp_init_lock(&lock);
    #endif

    double time_start, time_end, duration;
    time_start = omp_get_wtime();

    #if defined(MODE_REDUCTION)
	#pragma omp parallel shared(a, b) private(i, curr_prod) reduction (+:res)
	#else
    // Взаимоисключение
    #pragma omp parallel shared(a, b, res) private(i, curr_prod)
	#endif
    {
        #pragma omp for
        for (i=0; i < n; i++) {
            curr_prod = a[i] * b[i];

            #if defined(MODE_REDUCTION)
            res = res + curr_prod;
            #elif defined(MODE_ATOMIC)
            #pragma omp atomic
            res = res + curr_prod;
            #elif defined(MODE_CRITICAL)
            #pragma omp critical
            {
                res = res + curr_prod;
            }
            #elif defined(MODE_LOCK)
            omp_set_lock (&lock);
            res = res + curr_prod;
            omp_unset_lock (&lock);
            #endif
        }
    }
    time_end = omp_get_wtime();
    duration = time_end - time_start;

    #if defined(MODE_LOCK)
    omp_destroy_lock (&lock);
    #endif

    /* Вывод программы */
    // cout << "Size of vectors: " << n << endl;
    // cout << "Result: " << res << endl;
	// cout << "Time: " << duration << " s" << endl;
    cout << duration << endl;

    return 0;
}
