import pandas as pd
import matplotlib.pyplot as plt
    
def plot_mode():
    df = pd.read_csv('benchmarks/task2_6_out.csv')
    df = df.pivot(index='nprocs', columns='mode', values='ex_time')
    df.plot(
        figsize=(10, 5),
        xlabel='Кол-во потоков', ylabel='Время работы (сек)')
    plt.savefig('benchmarks/task2_6_out.png')
    plt.show()

if __name__ == '__main__':
    plot_mode()
