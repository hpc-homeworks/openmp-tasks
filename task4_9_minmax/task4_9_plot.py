import pandas as pd
import matplotlib.pyplot as plt
    
def plot_nested():
    df = pd.read_csv('benchmarks/task4_9_out_nested.csv')
    df.plot(
        figsize=(10, 5),
        x='nprocs', y='ex_time',
        xlabel='Кол-во потоков', ylabel='Время работы (сек)')
    plt.savefig('benchmarks/task4_9_out_nested.png')
    plt.show()

if __name__ == '__main__':
    plot_nested()
