#!/bin/sh
set -ex

module load gcc/9

omp_num_threads=8
max_threads=16
path_to_code=$PWD/task4_9_minmax.cpp
path_to_exe=$PWD/task4_9_minmax.exe
out_nested_csv_file=$PWD/benchmarks/task4_9_out_nested.csv
out_compare_csv_file=$PWD/benchmarks/task4_9_out_compare.csv

bench_nested_threads(){
    # Set up out file
    if [[ -f "$out_nested_csv_file" ]]
    then
        rm $out_nested_csv_file  
    fi
    printf '%s\n' "nprocs,ex_time" >> $out_nested_csv_file

    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    # Run
    export N=10000
    export OMP_NESTED="True"
    for nprocs in $(seq 1 $max_threads)
    do
        export OMP_NUM_THREADS=$nprocs
        ex_time=$( $path_to_exe )
        printf "%s\n" "$nprocs,$ex_time" >> $out_nested_csv_file
    done
}

bench_compare(){
    # Set up out file
    if [[ -f "$out_compare_csv_file" ]]
    then
        rm $out_compare_csv_file
    fi
    printf '%s\n' "n,with_nested,ex_time" >> $out_compare_csv_file

    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    export OMP_NUM_THREADS=$omp_num_threads

    # Run
    export OMP_NESTED="False"
    for n in $(seq 1000 1000 10000)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,0,$ex_time" >> $out_compare_csv_file
    done

    # Build
    g++ -fopenmp -g $path_to_code -o $path_to_exe

    # Run
    export OMP_NESTED="True"
    for n in $(seq 1000 1000 10000)
    do
        export N=$n
        ex_time=$( $path_to_exe )
        printf "%s\n" "$n,1,$ex_time" >> $out_compare_csv_file
    done

    rm $path_to_exe
}

bench_nested_threads
# bench_n
