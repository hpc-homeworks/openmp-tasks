#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <cstdlib>
#include <omp.h>

#define DEF_N 1000

using namespace std;

int main()
{
    int n = DEF_N;
    char * ENV_N = getenv("N");
    if (ENV_N)
        n = atoi(ENV_N);

    int i;   
    float curr_prod, res = 0.0;

    /* Инициализация данных */
    float *a = new float[n];
    float *b = new float[n];
    for (i=0; i < n; i++){
        a[i] = rand() % 100;
        b[i] = rand() % 100;
    }
    
    double time_start, time_end, duration;
    #ifdef _OPENMP
        time_start = omp_get_wtime();
    #else
        time_start = clock();
    #endif
     
    #pragma omp parallel shared(a, b) private(i, curr_prod) reduction (+:res)
    {
        #pragma omp for
        for (i=0; i < n; i++) {
            curr_prod = a[i] * b[i];
            res = res + curr_prod;         
        }
    }
    #ifdef _OPENMP
        time_end = omp_get_wtime();
        duration = time_end - time_start;
    #else
        time_end = clock();
        duration = (time_end - time_start) / CLOCKS_PER_SEC;
    #endif

    /* Вывод программы */
    // cout << "Size of vectors: " << n << endl;
    // cout << "Result: " << res << endl;
	// cout << "Time: " << duration << " s" << endl;
    cout << duration << endl;

    return 0;
}
